from django.test import TestCase, LiveServerTestCase
from django.test.client import Client
from django.urls import resolve
from django.apps import apps
from .views import *
from homepage.apps import HomepageConfig
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import unittest
import time

# Create your tests here.

class UnitTest(TestCase):
    def testHomepage(self):
        response = self.client.get('/')
        self.assertEquals(response.status_code, 200)
    
    def testCorrectTemplate(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')
'''
class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        
        #self.browser = webdriver.Firefox()
        
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome(chrome_options=chrome_options, executable_path='chromedriver')
        
    def tearDown(self):
        
        self.browser.quit()

    def test_search_script(self):
        self.browser.get(self.live_server_url)
        time.sleep(5)

        self.browser.find_element_by_id("searchBox").send_keys("Trying Again")
        time.sleep(10)
        self.browser.find_element_by_id("searchButton").click()
        time.sleep(10)
        self.assertIn(self.browser.find_element_by_id("idBook0"),"Trying Again")
'''
